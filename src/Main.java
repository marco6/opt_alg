import java.io.IOException;
import java.sql.Time;

import solver.*;

public class Main {

	public static void main(String[] args) throws IOException {
		String[] files = { /*"C101.txt", "C102.txt", "C103.txt", "C104.txt", "C105.txt", "C106.txt", "C107.txt",
				"C108.txt", "C109.txt", "C201.txt", "C202.txt", "C203.txt", "C204.txt", "C205.txt","C206.txt",
				"C207.txt", "C208.txt",*/ "RC201.txt", "RC202.txt", "RC203.txt", "RC204.txt", "RC205.txt", "RC206.txt",
				"RC207.txt", "RC208.txt" };
		for(String file : files)
			run(file);
	}

	public static void run(String filename) throws IOException {
		Problem p = new Problem("input/" + filename, 10);
		System.out.println(filename);
		Solution best = new Solution(p);
		best.unused.addAll(p.getNodes()); 
		Recreate.bestRecreate(best);

		long time = System.currentTimeMillis();

		for(int i = 0; i < 89000; i++){
			Solution act = Ruin.RandomRuin(best, 0.6);
			Recreate.bestRecreate(act);
			if(act.unused.size() < best.unused.size() || (act.unused.size() == best.unused.size() && act.cost < best.cost)){
				best = act;
//				System.out.println("Soluzione trovata(" + i + ")! Costo:" + best.cost + ", UnusedJobs: " + best.unused.size());
			}
		}
		
		System.out.println("Millisecondi impiegati: " + (System.currentTimeMillis() - time));
		
        System.out.println("\nTOTAL COST => " + best.cost +"\n"
                + "Unassigned Jobs => "+ best.unused.size()+"\n");
        int i=0;
        for(Route route : best.routes)
        	if(!route.isEmpty())
        		i++;
//        		System.out.println("Rotta " + i++ + " => Distance: " + route.getCost() +" Jobs: "+ route.size());
        System.out.println("Used routes: " + i);
	}
	
}
