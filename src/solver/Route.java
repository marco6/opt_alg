package solver;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

public class Route extends ArrayList<Node> {

	private final Problem prob;
	private int capacityLeft;

	public Route(Problem p) {
		prob = p;
		capacityLeft = p.capacity;
	}

	public Route(Route r) {
		prob = r.prob;
		capacityLeft = r.capacityLeft;
		this.addAll(r);
	}

	private static final long serialVersionUID = 7089595262232195063L;

	@Override
	public Object clone() {
		return new Route(this);
	}

	@Override
	public boolean add(Node n) {
		if (capacityLeft < n.demand)
			return false;
		capacityLeft -= n.demand;
		return super.add(n);
	}

	@Override
	public void add(int ix, Node n) {
		assert (capacityLeft >= n.demand);
		capacityLeft -= n.demand;
		super.add(ix, n);
	}

	@Override
	public Node remove(int ix) {
		Node r = super.remove(ix);
		capacityLeft += r.demand;
		return r;
	}

	@Override
	public boolean remove(Object o) {
		if (o instanceof Node && super.remove(o)) {
			capacityLeft += ((Node) o).demand;
			return true;
		}
		return false;
	}
	
	public Pair<Double, Integer> evalInsert(Node ins) {
		int index = -1;
		double cost = Double.MAX_VALUE;
		
		ArrayList<Node> visit = new ArrayList<>(this);
		visit.add(prob.startNode);
		
		if (capacityLeft >= ins.demand) {
			Node before = prob.startNode;
			for (int i = 0; i < visit.size(); i++) {
				Node act = visit.get(i);

				double actCost = NodeDistance.compute(before, ins) + NodeDistance.compute(ins, act) - NodeDistance.compute(before, act);

				//FIXME fa schifo
				this.add(i, ins);
				boolean valid = valid();
				this.remove(i);
				
				// Ora devo vedere se posso metterlo!
				if (valid  && actCost < cost) {
					index = i;
					cost = actCost;
				}
				before = act;
			}
		}
		
		return new Pair<>(cost, index);
	}

	public boolean valid() {
		int time = prob.startNode.readyTime + prob.startNode.serviceTime;
		Node prev = prob.startNode;
		for(Node n : this) {
			time += NodeDistance.compute(prev, n);
			time = Math.max(time, n.readyTime);
			if(time > n.dueTime)
				return false;
			time += n.serviceTime;
			prev = n;
		}
		return (time + NodeDistance.compute(prev, prob.startNode)) < prob.startNode.dueTime;
	}
	
	public double getCost(){
		double cost = 0;
		ArrayList<Node> nodes = new ArrayList<>(this);
		nodes.add(prob.startNode);
		Node first = prob.startNode;
		for(Node second : nodes) {
			cost += NodeDistance.compute(first, second);			
			first = second;
		}
		return cost;
	}

}
