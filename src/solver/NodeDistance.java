package solver;

import java.util.Comparator;

public class NodeDistance implements Comparator<Node> {

	public static double compute(Node a, Node b){
		int x = a.x - b.x, y = a.y - b.y;
		return Math.sqrt(x * x + y * y);
	}
	
	private final Node centroid;
	
	protected NodeDistance(Node centroid) {
		super();
		this.centroid = centroid;
	}

	@Override
	public int compare(Node o1, Node o2) {
		return (int)Math.signum(compute(o1, centroid) - compute(o2, centroid));
	}

}
