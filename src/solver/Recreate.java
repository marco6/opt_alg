package solver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.ListIterator;

public class Recreate {
	public static void bestRecreate(Solution s){
		Collections.shuffle(s.unused);
		ArrayList<Node> rUnused = new ArrayList<>();
		for(Node n : s.unused) {
			double bestCost = Double.MAX_VALUE;
			Route bestRoute = null;
			int bestIndex = 0;
			for(Route r : s.routes) {
				Pair<Double, Integer> p = r.evalInsert(n);

				if(bestCost > p.first) {
					bestCost = p.first;
					bestIndex = p.second;
					bestRoute = r;
				}
			}
			if(bestRoute != null) 
				bestRoute.add(bestIndex, n);
			else
				rUnused.add(n);
		}
		
		s.cost = 0;
		for(Route r : s.routes) {
			s.cost += r.getCost();
		}
		
		s.unused = rUnused;
	}
}
