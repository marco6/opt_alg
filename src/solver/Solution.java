package solver;

import java.util.ArrayList;
import java.util.Collections;

public class Solution implements Cloneable {
	
	public final Problem problem;
	
	public ArrayList<Route> routes;
	public double cost;
	public ArrayList<Node> unused;
	
	public Solution(Problem p) {
		super();
		problem = p;
		routes = new ArrayList<>(p.vehicles);
		for(int i = 0; i < p.vehicles; i++)
			routes.add(new Route(p)); 
		unused = new ArrayList<>();
		cost = 0;
	}

	@SuppressWarnings("unchecked")
	public Solution(Problem p, ArrayList<Route> routes, double cost, ArrayList<Node> unused) {
		super();
		this.problem = p;
		this.routes = new ArrayList<Route>(routes.size());
		
		for(Route r : routes){
			this.routes.add((Route)r.clone());
		}
		
		this.cost = cost;
		this.unused = (ArrayList<Node>)unused.clone();
	}
	
	public Object clone() {
		return new Solution(problem, routes, cost, unused);
	}
	
}