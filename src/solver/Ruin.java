package solver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Random;

public class Ruin {
	
	private static Random rand = new Random(System.currentTimeMillis());
	
	public static Solution RandomRuin(Solution s, double amount) {
		// Rovino a cazzo robba!
		Solution ruined = (Solution)s.clone();

		List<Node> nodes = new ArrayList<Node>(s.problem.getNodes());
		Collections.shuffle(nodes);

		int nR = (int)(amount * nodes.size());
		Iterator<Node> it = nodes.iterator();

		while(nR > 0 && it.hasNext()) {
			Node n = it.next();
			for(Route r : ruined.routes){
				if(r.remove(n)){
					ruined.unused.add(n);
					break;
				}
			}
			nR--;
		}
		
		return ruined;
	}
	
	public static Solution RadialRuin(Solution s, double amount){
		Solution ruined = (Solution)s.clone();
		List<Node> nodes = s.problem.getNodes();
		Node victim = nodes.get(rand.nextInt(nodes.size()));
		PriorityQueue<Node> q = new PriorityQueue<>(s.problem.getNodes().size(), new NodeDistance(victim));
		q.addAll(nodes);
		
		int nR = (int)(amount * nodes.size());
		
		Iterator<Node> it = q.iterator();

		while(nR > 0 && it.hasNext()) {
			Node n = it.next();
			for(Route r : ruined.routes){
				if(r.remove(n)){
					ruined.unused.add(n);
					break;
				}
			}
			nR--;
		}
		
		return ruined;
		
	}
}