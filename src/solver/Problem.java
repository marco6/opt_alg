package solver;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

/**
 * Ecco la ricetta:
 * - Un solo tipo veicolo (e quindi nessuno),
 * -- Una sola capacit�
 * -- No abilit�
 * -- No tipo di conducente
 * @author Marco
 *
 */
public class Problem {
	public final int capacity, vehicles;

	public final Node startNode;
	
	private final ArrayList<Node> nodes;
	
	public final String name;

	
	public Problem(String fileName, int vehicles) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		nodes = new ArrayList<>(100); // O 100 o 200 in qualsiasi caso questo aiuta ad allocare meno!
		Iterator<String> it = br.lines().iterator();
		
		String name = null;
		int capacity = 0;
		int c = 0;
		while(it.hasNext()){
			++c;
			String line = it.next().trim();
			String[] tokens = line.split(" +");
			switch(c) {
			case 1:
				name = line;
				break;
			case 5:
				capacity = Integer.parseInt(tokens[1]);
				break;
			default:
				if(c >= 10){
					nodes.add(new Node(
							Integer.parseInt(tokens[0]), 
							Integer.parseInt(tokens[1]), 
							Integer.parseInt(tokens[2]), 
							Integer.parseInt(tokens[3]), 
							Integer.parseInt(tokens[4]), 
							Integer.parseInt(tokens[5]), 
							Integer.parseInt(tokens[6])));
				}
			}
			
		}
		
		this.name = name;
		this.capacity = capacity;
		this.vehicles = vehicles;
		this.startNode = nodes.remove(0);		
		
		br.close();
	}
	
	public List<Node> getNodes() {
		return Collections.unmodifiableList(nodes);
	}
}
