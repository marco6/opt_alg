package solver;

public class Node {
	public final int C_ID, x, y, demand, readyTime, dueTime,  serviceTime;

	public Node(int c_ID, int x, int y, int demand, int readyTime, int dueTime, int serviceTime) {
		super();
		C_ID = c_ID;
		this.x = x;
		this.y = y;
		this.demand = demand;
		this.readyTime = readyTime;
		this.dueTime = dueTime;
		this.serviceTime = serviceTime;
	}
	
}
